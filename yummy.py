#! /usr/bin/env python
import sqlite3

def main():
    whitelist = ["%bitbucket.org", "%python.org", "%debian.org"]

    sql = "DELETE FROM cookies WHERE"
    for domain in whitelist:
        sql += " host_key NOT LIKE '{0}' AND".format(domain)
    sql = sql[:-4]

    cookies_db = "/home/user/.config/google-chrome/Default/Cookies"
    conn = sqlite3.connect(cookies_db)
    cursor = conn.cursor()
    result = cursor.execute(sql)
    conn.commit()
    conn.close()
main()
